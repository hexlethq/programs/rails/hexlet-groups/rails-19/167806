# frozen_string_literal: true

require_relative 'test_helper'
require_relative '../lib/stack'

class StackTest < Minitest::Test
  # BEGIN
  def before_setup
    @stack = Stack.new([1, 2, 3])
  end

  def test_to_a
    expected_result = [1, 2, 3]
    assert_instance_of Array, @stack.to_a
    assert_equal expected_result, @stack.to_a
  end

  def test_pop!
    expected_result = 3
    assert_equal expected_result, @stack.pop!
    assert_equal @stack.size, 2
  end

  def test_push!
    expected_result = [1, 2, 3, 4]
    assert_equal expected_result, @stack.push!(4)
  end

  def test_empty?
    assert_equal @stack.size.zero?, @stack.empty?
    @stack.clear!
    assert_equal @stack.size.zero?, @stack.empty?
  end

  def test_clear!
    assert_equal @stack.empty?, false
    @stack.clear!
    assert_equal @stack.empty?, true
  end

  def test_size
    assert_equal @stack.size, 3
    @stack.clear!
    assert_equal @stack.size, 0
    @stack.push!(1)
    assert_equal @stack.size, 1
  end
  # END
end

test_methods = StackTest.new({}).methods.select { |method| method.start_with? 'test_' }
raise 'StackTest has not tests!' if test_methods.empty?

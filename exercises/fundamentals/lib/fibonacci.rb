# frozen_string_literal: true

# BEGIN
def fibonacci(num)
  if num.negative?
    nil
  else
    fibonacci_iter(0, 1, num)
  end
end

def fibonacci_iter(num1, num2, count)
  if count.zero?
    num2
  else
    fibonacci_iter(num1 + num2, num1, count - 1)
  end
end
# END

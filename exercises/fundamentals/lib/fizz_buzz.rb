# frozen_string_literal: true

# BEGIN
def fizz_buzz(start, stop)
  result = ''

  return start.to_s if start == stop

  start.upto(stop) do |i|
    result +=
      if (i % 3).zero? && (i % 5).zero?
        'FizzBuzz '
      elsif (i % 3).zero?
        'Fizz '
      elsif (i % 5).zero?
        'Buzz '
      else
        "#{i} "
      end
  end
  result.strip
end
# END

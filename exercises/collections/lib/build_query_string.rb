# frozen_string_literal: true

# BEGIN
def build_query_string(hash)
  hash.sort.map { |key, value| "#{key}=#{value}" }.join('&')
end
# END

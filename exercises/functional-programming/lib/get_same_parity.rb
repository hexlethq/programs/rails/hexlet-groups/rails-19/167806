# frozen_string_literal: true

# BEGIN
def get_same_parity(arr)
  return arr if arr.empty?

  parity = arr.first.even? ? :even? : :odd?
  arr.filter(&parity)
end
# END

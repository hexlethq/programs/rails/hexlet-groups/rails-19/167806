# frozen_string_literal: true

# BEGIN
def count_by_years(users)
  men = users.filter { |user| user[:gender] == 'male' }
  years = men.map { |item| Time.new(item[:birthday]).year.to_s }

  years.each_with_object({}) do |year, acc|
    acc[year] ||= 0
    acc[year] += 1
  end
end
# END

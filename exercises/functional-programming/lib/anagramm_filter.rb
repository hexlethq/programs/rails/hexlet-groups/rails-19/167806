# frozen_string_literal: true

# BEGIN
def anagramm_filter(word, list)
  list.filter { |item| item.chars.sort == word.chars.sort }
end
# END
